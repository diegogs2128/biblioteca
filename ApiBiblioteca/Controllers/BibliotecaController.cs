﻿using Aplicacion.Interfaces;
using Dominio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiBiblioteca.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BibliotecaController : ControllerBase
    {
        private IBiblioteca biblioteca;
        public BibliotecaController(IBiblioteca _biblioteca)
        {
            biblioteca = _biblioteca;
        }

        public ActionResult<List<Cartilla>> GetCartilla()
        {
            var Obj = biblioteca.GetCartilla();


            return Obj;
        }

        [HttpPost]
        public ActionResult<string> Metodos(Cartilla Obj)
        {
            string Ok = "";
            try
            {
                biblioteca.Metodos(Obj);
                Ok = "OK";
            }
            catch (Exception)
            {
                Ok = "Error";
            }
            return Ok;
        }

        [HttpGet("cartilla/{id}")]
        public Cartilla Cartilla(int id)
        {
            Cartilla obj = new Cartilla();
            obj = biblioteca.GetCartilla(id);
            return obj;

        }

    }
}
