﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aplicacion.Interfaces
{
   public interface IBiblioteca
    {
        List<Cartilla> GetCartilla();
        Cartilla GetCartilla(int Id);
        void Metodos(Cartilla Obj);
    }
}
