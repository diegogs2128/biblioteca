﻿using Aplicacion.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Dominio;
using Datos;

namespace Aplicacion.Gestion
{
   public class GesBiblioteca: IBiblioteca
    {
        public List<Dominio.Cartilla> GetCartilla()
        {
            List<Cartilla> Arr = new List<Cartilla>();
            using (SqlConnection c = new SqlConnection(Configuracion.Conexion))
            {
                if (c.State == ConnectionState.Closed) c.Open();
                DaoBiblioteca dao = new DaoBiblioteca(c);
                Arr = dao.CartillaGet();
            }
            return Arr;
        }

        public Dominio.Cartilla GetCartilla(int Id)
        {
            Cartilla Arr = new Cartilla();
            using (SqlConnection c = new SqlConnection(Configuracion.Conexion))
            {
                if (c.State == ConnectionState.Closed) c.Open();
                DaoBiblioteca dao = new DaoBiblioteca(c);
                Arr = dao.CartillaGet(Id);
            }
            return Arr;
        }


        public void Metodos(Cartilla Obj)
        {
            using (SqlConnection c = new SqlConnection(Configuracion.Conexion))
            {
                if (c.State == ConnectionState.Closed) c.Open();
                DaoBiblioteca dao = new DaoBiblioteca(c);
                dao.Metodos(Obj);
            }
        }
    }
}
