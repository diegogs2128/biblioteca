﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio
{
    public class Cartilla
    {
        public int IdBiblioteca { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Ruta { get; set; }
        public string BtnTex { get; set; }
        public string Enlace { get; set; }
        public Status status { get; set; }
        public enum Status
        {
            insert=1,
            update=2,
            delete=3
        }
    }
}
