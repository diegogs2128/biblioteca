﻿using Dapper;
using Dominio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
namespace Datos
{
    public class DaoBiblioteca
    {
        private SqlConnection c;
        public DaoBiblioteca(SqlConnection c)
        {
            this.c = c;
        }

        public List<Cartilla> CartillaGet()
        {
            List<Cartilla> Arr = null;
            Arr = c.Query<Cartilla>("Select * from Cartilla").ToList();
            return Arr;
        }
        public Cartilla CartillaGet(int Id)
        {
            Cartilla Arr = null;
            Arr = c.Query<Cartilla>($"Select * from Cartilla where IdBiblioteca={Id}").FirstOrDefault();
            return Arr;
        }
        public void Metodos(Cartilla obj)
        {
            string Cadena = "";
            try
            {
                switch (obj.status)
                {
                    case Cartilla.Status.insert:
                        Cadena += $@"INSERT INTO  Cartilla 
           (Titulo
           , Descripcion
           , Ruta
           , BtnTex
           , Enlace)
     VALUES (@Titulo
           , @Descripcion
           , @Ruta
           , @BtnTex
           , @Enlace)";
                        break;

                    case Cartilla.Status.update:
                        Cadena += $@"UPDATE Cartilla
   SET Titulo = @Titulo
      ,Descripcion = @Descripcion
      ,Ruta = @Ruta
      ,BtnTex = @BtnTex
      ,Enlace = @Enlace
 WHERE IdBiblioteca=@IdBiblioteca";
                        break;

                    case Cartilla.Status.delete:
                        Cadena += "Delete from Cartilla where IdBiblioteca=@IdBiblioteca";
                        break;
                }
                DynamicParameters P = new DynamicParameters(new
                {
                    Titulo = obj.Titulo
           ,
                    Descripcion = obj.Descripcion
           ,
                    Ruta = obj.Ruta
           ,
                    BtnTex = obj.BtnTex
           ,
                    Enlace = obj.Enlace
                    ,
                    IdBiblioteca = obj.IdBiblioteca

                });
                c.Execute(Cadena, P);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
